import React, {Component} from 'react'
import {Card, Grid, Icon, Image, Segment,} from 'semantic-ui-react'
import './books.css';
import ExactBooks from './exactBook/exact_book'
import Button from "semantic-ui-react/dist/es/elements/Button/Button";
class Books_List extends Component {
    render() {
        const {books,deleteBooks,genreName} = this.props;
        if(books.length === 0){
            return(
                <Segment>
                    <b>Извините, по вашему запросу  не  осталось книг</b>
                </Segment>
            )
        }
        else {
            return (
                <div className="booksContent">
                    <Card.Group itemsPerRow={4}>
                        {books && books.map((books_list) => {
                            return (
                                <ExactBooks bookslist={books_list} deleteBooks={deleteBooks}/>
                            )
                        })}
                    </Card.Group>
                </div>
            )
        }
    }
}
export default Books_List;