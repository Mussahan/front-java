import React, {Component} from 'react'
import Input from "semantic-ui-react/dist/commonjs/elements/Input";
import Button from "semantic-ui-react/dist/commonjs/elements/Button";
import Container from "semantic-ui-react/dist/es/elements/Container/Container";
import SaveImage from './saveimage'
import axios from "axios";
import {Dropdown, Select} from "semantic-ui-react";


class addBooks extends Component {
    constructor(props) {
        super(props);
        this.state={
            bookName:null,
            bookYear:null,
            bookInformation:null,
            bookAuthor:null,
            bookGenre:null,
            file: '',
            error: '',
            msg: '',
        }
    }
    handleName = (e) =>{
        this.setState({
            bookName: e.target.value
        })
    }
    handleYear = (e) =>{
        this.setState({
            bookYear:e.target.value
        })
    }
    handleDescription = (e) => {
        this.setState({
            bookInformation:e.target.value
        })
    }
    handleAuthor = (e) => {
        this.setState({
            bookAuthor:e.target.value
        })
    }
    handleGenre = (e, { name, value }) => this.setState({ [name]: value })

    handleSubmit =(e) =>{
        this.props.addBooks(this.state);
        this.setState({
            bookName:'',
            bookYear:'',
            bookInformation:'',
            bookAuthor:''
        })

        axios.post('http://localhost:8080/api/books',{
            bookName:this.state.bookName,
            bookYear:this.state.bookYear,
            bookInformation:this.state.bookInformation,
            bookAuthor:this.state.bookAuthor,
            bookUrl:this.state.file.name,
            bookGenre:this.state.bookGenre,
            bookPk:this.state.bookGenre,
        })
        this.setState({error: '', msg: ''});
        if(!this.state.file) {
            this.setState({error: 'Please upload a file.'})
            return;
        }
        if(this.state.file.size >= 2000000) {
            this.setState({error: 'File size exceeds limit of 2MB.'})
            return;
        }
        let data = new FormData();
        data.append('file', this.state.file);
        data.append('name', this.state.file.name);

        fetch('http://localhost:8080/api/files', {
            method: 'POST',
            body: data
        }).then(response => {
            this.setState({error: '', msg: 'Sucessfully uploaded file'});
        }).catch(err => {
            this.setState({error: err});
        });
    }

    onFileChange = (event) => {
        this.setState({
            file: event.target.files[0]
        });
    }
    renderOptions =()=>{
        if(this.props.genres !== null && this.props.genres !== undefined) {
            var array = []
            const values = this.props.genres.map((val,key) => {
                array.push({key:key,text:val.genreName,value:val.id})
            })
            return array;
        }
    }

    render() {
        const options = this.renderOptions();
        const {genres} = this.props
        return (
            <div className="input-books">
                <form onSubmit={(this.handleSubmit)} className={'formochka'}>
                    <h3>Upload books</h3>
                    <br/><Input onChange={this.handleName} value={this.state.bookName} placeholder='book name' className={'form-element_addbooks'}/>
                    <br/><Input onChange={this.handleYear} value={this.state.bookYear} placeholder='book year' className={'form-element_addbooks'}/>
                    <br/><Input onChange={this.handleDescription} value={this.state.bookInformation} placeholder='book description' className={'form-element_addbooks'}/>
                    <br/><Input onChange={this.handleAuthor} value={this.state.bookAuthor} placeholder='book Author' className={'form-element_addbooks'}/>
                    <br/><div className={'form-element_addbooks'}><Dropdown selection name='bookGenre' options={options} placeholder='Select genre' onChange={this.handleGenre}/></div>
                    <input style={{display:"none"}} onChange={this.onFileChange} type="file" className={'form-element_addbooks'}
                        ref={fileInput=>this.fileInput = fileInput}
                    />
                    <br/>
                    <Button onClick={(e)=>{this.fileInput.click();e.preventDefault()}}>Select image</Button>
                    <Button className={'form-element_addbooks'} positive>Upload Book</Button>
                </form>
            </div>
        );
    }
}
export default addBooks;