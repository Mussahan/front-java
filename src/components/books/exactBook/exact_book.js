import React , {Component} from 'react'
import {Card, Grid, Icon, Image, Button, Modal, Header} from 'semantic-ui-react'
import axios from "axios";
import image from '../../../images/aidy2cBlOdk.jpg'
import NestedModal from '../NestedModal';
class ExactBook extends Component {

    state = { open: false }

    show = dimmer => () => this.setState({ dimmer, open: true })
    close = () => this.setState({ open: false })

    getImageUrl =()=>{
        const {bookslist} = this.props;
        if(bookslist.bookUrl === null || bookslist.bookUrl === undefined ){
            return require('../../../images/def.png')
        }
        else {
            return require('../../../images/'+bookslist.bookUrl);
        }
    }
    renderCard = (bookslist)=>{
        return(
            <Card color='red' image={this.getImageUrl()} header={bookslist.bookName} onClick={this.show('blurring')}/>
        )
    }
    renderGenreName =(bookslist)=>{
        try {
            return(<p><b>Жанр : </b>{bookslist.genreList.genreName}</p>)
        }
        catch (e) {
            console.log('error',e)
        }

    }
    render() {
        const {bookslist,bookgenre} = this.props;
        const { open, dimmer } = this.state
        return(
            <div className={"singleElement"}>
                <Modal dimmer={dimmer} open={open} onClose={this.close}>
                    <Modal.Header>{bookslist.bookName}</Modal.Header>
                    <Modal.Content image>
                        <Image wrapped size='medium' src={this.getImageUrl()} />
                        <Modal.Description>
                            <Header><b>Автор: </b>{bookslist.bookAuthor}</Header>
                            <p><b>Информация: </b>{bookslist.bookInformation}</p>
                            {this.renderGenreName(bookslist)}
                            <p><b>Год издания :</b> {bookslist.bookYear}</p>
                        </Modal.Description>
                    </Modal.Content>
                    <Modal.Actions>
                        <NestedModal delete={this.props.deleteBooks} id={bookslist.id}/>
                    </Modal.Actions>
                </Modal>
                {this.renderCard(bookslist)}
            </div>
        )
    }
}
export default ExactBook;