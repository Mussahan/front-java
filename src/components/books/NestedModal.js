import React, { Component } from 'react'
import { Button, Icon, Modal } from 'semantic-ui-react'

class NestedModal extends Component {
    state = { open: false }

    open = () => this.setState({ open: true })
    close = () => this.setState({ open: false })

    render() {
        const { open } = this.state

        return (
            <Modal
                open={open}
                onOpen={this.open}
                onClose={this.close}
                size='small'
                trigger={
                    <Button danger >
                        Удалить запись
                    </Button>
                }
            >
                <Modal.Header>Вы уверены, что хотите удалить?</Modal.Header>
                <Modal.Actions>
                    <Button
                        positive
                        icon='checkmark'
                        labelPosition='right'
                        content="Нет"
                        onClick={this.close}
                    />
                    <Button negative onClick={()=>{this.close();this.props.delete(this.props.id)}}>
                        Удалить запись
                    </Button>
                </Modal.Actions>
            </Modal>
        )
    }
}
export default NestedModal;