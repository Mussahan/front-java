import React, {Component} from 'react'
import UserTable from '../../components/admin/user_table'

class Student_list extends Component {

    render() {
        const {handleDelete} = this.props;
        return(
            this.props.students && this.props.students.map((bookslist,key)=>{
            return(
                <UserTable booklist={bookslist} handleDelete={handleDelete}/>
            )
        })
        )
    }
}
export default Student_list;