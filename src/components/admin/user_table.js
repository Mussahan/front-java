import React,{Component} from 'react'
import {Button, Container, Table} from 'semantic-ui-react'
import {Modal} from "semantic-ui-react";
import AddStudent from "../../views/admin/admin";


class UserTable extends Component{
    state = { open: false }

    open = () => this.setState({ open: true })
    close = () => this.setState({ open: false })

    render() {
        const { open } = this.state

        return (

            <Table.Body>
                <Table.Row>
                    <Table.Cell>{this.props.booklist.id}</Table.Cell>
                    <Table.Cell>{this.props.booklist.studentName}</Table.Cell>
                    <Table.Cell>{this.props.booklist.studentEmail}</Table.Cell>
                    <Table.Cell>{this.props.booklist.studentAge}</Table.Cell>
                    <Table.Cell>{this.props.booklist.phoneNumber}</Table.Cell>
                    <Table.Cell>
                        <Modal
                            open={open}
                            onOpen={this.open}
                            onClose={this.close}
                            size='small'
                            trigger={
                                <Button negative >
                                    X
                                </Button>
                            }
                        >
                            <Modal.Header>Вы уверены, что хотите удалить?</Modal.Header>
                            <Modal.Actions>
                                <Button
                                    positive
                                    icon='checkmark'
                                    labelPosition='right'
                                    content="Нет"
                                    onClick={this.close}
                                />
                                <Button negative onClick={()=>{this.close();
                                    this.props.handleDelete(this.props.booklist.id)}}>X</Button>
                            </Modal.Actions>
                        </Modal>
                    </Table.Cell>
                </Table.Row>
            </Table.Body>

        );
    }
}
export default UserTable;