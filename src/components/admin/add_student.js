import React, {Component} from 'react'
import Input from "semantic-ui-react/dist/commonjs/elements/Input";
import Button from "semantic-ui-react/dist/commonjs/elements/Button";
import axios from "axios";
import {Segment} from "semantic-ui-react";
import {Redirect} from "react-router-dom";


class addStudent extends Component {
    constructor(props) {
        super(props);
        this.state={
            studentName:null,
            studentAge:null,
            studentEmail:null,
            phoneNumber:null,
            redirect: false
        }
    }
    handleName = (e) =>{
        this.setState({
            studentName: e.target.value
        })
    }
    handleAge = (e) =>{
        this.setState({
            studentAge:e.target.value
        })
    }
    handleEmail = (e) => {
        this.setState({
            studentEmail:e.target.value
        })
    }
    handlePhoneNumber = (e) => {
        this.setState({
            phoneNumber:e.target.value
        })
    }

    setRedirect = () => {
        this.setState({
            redirect: true
        })
    }
    renderRedirect = () => {
        if (this.state.redirect) {
            return <Redirect to='/admin' />
        }
    }


    handleSubmit =(e) =>{
        e.preventDefault();
        console.log(this.state)

        axios.post('http://localhost:8080/api/student/save',{
            studentName:this.state.studentName,
            studentAge:this.state.studentAge,
            studentEmail:this.state.studentEmail,
            phoneNumber:this.state.phoneNumber,
        })
        this.setRedirect()
        // this.setState({
        //     studentName:'',
        //     studentAge:'',
        //     studentEmail:'',
        //     phoneNumber:'',
        // })

    }

    render() {
        return (
            <div className="input-books">
                <form onSubmit={(this.handleSubmit)} className={'formochka'}>
                    <h3>Upload students</h3>
                    <br/><Input onChange={this.handleName} value={this.state.studentName} placeholder='student name' className={'form-element_addbooks'}/>
                    <br/><Input onChange={this.handleAge} value={this.state.studentAge} placeholder='student age' className={'form-element_addbooks'}/>
                    <br/><Input onChange={this.handleEmail} value={this.state.studentEmail} placeholder='student email' className={'form-element_addbooks'}/>
                    <br/><Input onChange={this.handlePhoneNumber} value={this.state.phoneNumber} placeholder='student phone' className={'form-element_addbooks'}/>
                    <br/>
                    <Button className={'form-element_addbooks'} positive >Upload Student</Button>
                </form>
            </div>
        );
    }
}
export default addStudent;