import React, { Component } from 'react';
import 'semantic-ui-css/semantic.min.css'
import Header from './views/header/header'
import {BrowserRouter, Redirect} from "react-router-dom"
import Books from './views/books/books'
import Admin from './views/admin/admin'
import AddStudent from './components/admin/add_student'

import Container from "semantic-ui-react/dist/es/elements/Container/Container";
import {Route} from "react-router";
class App extends Component {
  render() {
    return (
      <div className="App">
      <BrowserRouter>
        <Container>
          <Header/>
          <Route exact path='/books' component={Books}/>
          <Route exact path='/student/add' component={AddStudent}/>
          <Route path='/admin' component={Admin}/>
        </Container>
      </BrowserRouter>
      </div>
    );
  }
}

export default App;
