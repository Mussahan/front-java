import React, {Component} from 'react'
import {Icon, Menu} from 'semantic-ui-react'
import {Link} from "react-router-dom";
import './header.css'

export default class MenuExampleStackable extends Component {
    state = {}
    handleItemClick = (e, { name }) => this.setState({ activeItem: name })
    render() {
        const { activeItem } = this.state

        return (
            <Menu stackable>
                <Menu.Item>
                    <img src='/favicon.ico' />
                </Menu.Item>
                <Link to='/books'>
                    <Menu.Item name='testimonials' active={activeItem === 'testimonials'} onClick={this.handleItemClick}>
                        <Icon name='book' />
                        Books
                    </Menu.Item>
                </Link>
                <Menu.Menu position='right'>
                <Link to='/admin'>
                    <Menu.Item position='right' name='features' active={activeItem === 'features'}  onClick={this.handleItemClick}>
                        <Icon name='adn' />
                        Admin
                    </Menu.Item>
                </Link>
                </Menu.Menu>
            </Menu>
        )
    }
}
