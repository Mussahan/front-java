import React, {Component} from 'react'
import {Container, Dimmer, Dropdown, Loader, Modal, Pagination, Segment} from 'semantic-ui-react'
import Books_list from '../../components/books/books'
import AddBooks from '../../components/books/addBooks'
import axios from "axios";
import Button from "semantic-ui-react/dist/es/elements/Button/Button";

class Books extends Component{
    constructor() {
        super()
        this.state = {
            books: [],
            activePage: 1,
            sort:null,
            totalPages:null,
            genreList:null,
            user:{
                user_id:1,
                user_name:"Aziz",
                user_role:"ADMIN"
            },
            genreHandler:null,
        }
    }
    addBooks =(book) => {
        book.id = this.state.books.length +1;
        let books = [[...this.state.books,book]];
        this.setState({
            books
        })
    }
    handleSort = (name) =>{
        this.setState({
            sort:name
        })
    }
    handleDelete = (id) => {
        const books = this.state.books.filter(book => {
            return book.id !== id
        });
        this.setState({
            books
        });
        axios.get('http://localhost:8080/api/books/delete/' + id,)
            .then(res => {
            })
    }


    componentDidMount() {
        let url = window.location.pathname;
        let pageBooks = this.state.activePage-1;
        axios.get('http://localhost:8080/api/'+url+'?page='+pageBooks).then(res=>{
            this.setState({
                books:res.data.content
            })
        })
        axios.get('http://localhost:8080/api/books/all').then(res=>{
            this.setState({
                totalPages:Math.round(res.data/5)
            })
        })
        axios.get('http://localhost:8080/api/genres').then(res=>{
            this.setState({
                genreList:res.data
            })
        })
    }
    handleActiveButton =(condition,title)=>{
        if(condition === this.state.sort){
            return(
                <Button onClick={()=>this.handleSort(condition)} color='green' >{title}</Button>
            )
        }
        else{
            return(
                <Button onClick={()=>this.handleSort(condition)} basic color='green' >{title}</Button>
            )
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        let url = window.location.pathname;
        let pageBooks = this.state.activePage-1;
        if(prevState.activePage !== this.state.activePage){
            this.setState({
                books:null
            })
            axios.get('http://localhost:8080/api'+url+'?page='+pageBooks).then(res=>{
                this.setState({
                    books:res.data.content
                })
            })
        }
        if(prevState.sort !== this.state.sort && this.state.genreHandler == null){
            this.setState({
                books:null
            })
            axios.get('http://localhost:8080/api'+url+'?sortBy='+this.state.sort+'&page='+pageBooks).then(res=>{
                this.setState({
                    books:res.data.content
                })
            })
        }
        else if(prevState.sort !== this.state.sort && this.state.genreHandler !== null){
            axios.get('http://localhost:8080/api/books/genres/'+this.state.genreHandler+'?page='+pageBooks+'&sortBy='+this.state.sort).then(res=>{
                this.setState({
                    books:res.data
                })
            })
        }
        else if(prevState.sort !== this.state.sort && this.state.genreHandler !== null){
            this.setState({
                books:null
            })
            axios.get('http://localhost:8080/api'+url+'?sortBy='+this.state.sort+'&page='+pageBooks).then(res=>{
                this.setState({
                    books:res.data.content
                })
            })
        }
        if(this.state.genreHandler !== null && prevState.genreHandler !== this.state.genreHandler && this.state.sort == null){
            axios.get('http://localhost:8080/api/books/genres/' + this.state.genreHandler + '?page=' + pageBooks).then(res => {
                this.setState({
                    books: res.data
                })
            })
        }
    }
    handlePaginationChange = (e, { activePage }) => {
        this.setState({ activePage })
    }

    renderOptions =()=>{
        if(this.state.genreList !== null && this.state.genreList !== undefined) {
            var array = []
            const values = this.state.genreList.map((val,key) => {
                array.push({key:key,text:val.genreName,value:val.id})
            })
            return array;
        }
    }
    handleGenre = (e, { name, value }) => this.setState({ [name]: value })

    handleInformation =()=>{
        const options = this.renderOptions();
        return(
            <div>
                <Segment>
                    {this.handleActiveButton("bookName","Book name")}
                    {this.handleActiveButton("bookYear","Book Year")}
                    {this.handleActiveButton("bookAuthor","Book Author")}
                    {this.handleActiveButton("bookInformation","Book Information")}
                    <Modal trigger={<Button floated='right' positive>Add book</Button>}>
                        <Modal.Header>Add a book</Modal.Header>
                        <Modal.Content>
                            <Container>
                                <AddBooks addBooks={this.addBooks} genres={this.state.genreList} />
                            </Container>
                        </Modal.Content>
                    </Modal>
                    <div><br/><Dropdown selection name='genreHandler' options={options} placeholder='Select genre' onChange={this.handleGenre}/></div>
                    <Books_list books={this.state.books } deleteBooks={this.handleDelete} />
                </Segment>
                <div className="pagination-center">
                    <Pagination
                        activePage={this.state.activePage}
                        onPageChange={this.handlePaginationChange}
                        totalPages={this.state.totalPages}
                    />
                </div>
            </div>
        )
    }
    handleLoader =()=>{
        return(
            <div>
                <Segment>
                    <Button onClick={()=>this.handleSort('bookName')}>Book name</Button>
                    <Button onClick={()=>this.handleSort('bookYear')}>Book Year</Button>
                    <Button onClick={()=>this.handleSort('bookAuthor')}>Book Author</Button>
                    <Button onClick={()=>this.handleSort('bookInformation')}>Book Information</Button>
                    <Dimmer active inverted>
                        <Loader size='small'>Loading</Loader>
                    </Dimmer>
                    <AddBooks addBooks={this.addBooks} />
                </Segment>
                <div className="pagination-center">
                    <Pagination
                        activePage={this.state.activePage}
                        onPageChange={this.handlePaginationChange}
                        totalPages={this.state.totalPages}
                    />
                </div>
            </div>
        )
    }
    render() {
        if(this.state.books === null){
            return(
                this.handleLoader()
            )
        }
        else {
            return (
                this.handleInformation()
            );
        }
    }
}

export default Books;