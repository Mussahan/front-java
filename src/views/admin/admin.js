import React, {Component} from 'react'
import {Button, Container, Modal, Pagination, Segment, Table} from 'semantic-ui-react'
import axios from 'axios'
import Users_list from '../../components/admin/students'
import {Link} from "react-router-dom";
import AddStudent from '../../components/admin/add_student'
import AddBooks from "../books/books";
class Students extends Component{

    constructor(props) {
        super(props);
        this.state ={
            students:[],
            activePage: 1,
            sort:null,
            totalPages:null,
            modalOpen: false

        }
    }

    handleOpen = () => this.setState({ modalOpen: true })

    handleClose = () => this.setState({ modalOpen: false })

    componentDidMount() {
        let url = window.location.pathname;
        let pageBooks = this.state.activePage-1;
        axios.get('http://localhost:8080/api/student?page='+pageBooks).then(res=>{
            console.log(res.data.content);
            this.setState({
                students:res.data.content
            })
        })
        axios.get('http://localhost:8080/api/students/all').then(res=>{
            this.setState({
                totalPages:Math.round(res.data/5)
            })
        })
    }
    componentDidUpdate(prevProps, prevState, snapshot) {
        let pageBooks = this.state.activePage-1;
        if(prevState.activePage !== this.state.activePage){
            this.setState({
                students:null
            })
            axios.get('http://localhost:8080/api/student?page='+pageBooks).then(res=>{
                this.setState({
                    students:res.data.content
                })
            })
        }
        if(this.state.students !== prevState.students){
            this.setState({

            })
        }
    }

    handleDelete = (id) => {
        const students = this.state.students.filter(students => {
            return students.id !== id
        });
        this.setState({
            students
        });
        axios.get('http://localhost:8080/api/student/delete/' + id,)
            .then(res => {
            })
    }

    handlePaginationChange = (e, { activePage }) => {
        this.setState({ activePage })
    }


    render() {
        return (
            <div>
                <Segment>
                    <Modal trigger={<Button floated='right' positive>Add student</Button>}>
                        <Modal.Header>Add a Student</Modal.Header>
                        <Modal.Content>
                            <Container>
                                <AddStudent />
                            </Container>
                        </Modal.Content>
                    </Modal>
                    <div>
                    <Table celled>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell>id</Table.HeaderCell>
                                <Table.HeaderCell>Name</Table.HeaderCell>
                                <Table.HeaderCell>Email</Table.HeaderCell>
                                <Table.HeaderCell>Age</Table.HeaderCell>
                                <Table.HeaderCell>Phone</Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>
                        <Users_list students={this.state.students} handleDelete={this.handleDelete}/>
                    </Table>
                    </div>
                    <div className="pagination-center">
                        <Pagination
                            activePage={this.state.activePage}
                            onPageChange={this.handlePaginationChange}
                            totalPages={this.state.totalPages}
                        />
                    </div>
                </Segment>
            </div>
        );
    }
}

export default Students;